import { Box, Button, TextField, Typography } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useContext, useState } from "react";
import { Link, useHistory, withRouter } from "react-router-dom";
import { ApplicationContext } from "../../../App";
import ViewWithModal from "../../../common/ViewWithModal";
import { LOGIN, QUIZES } from "../../../routes";
import AuthService from "../../../services/Auth.service";
import { useTranslation } from "react-i18next";

const Register = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const history = useHistory();
  const { userState, loadingState, errorState } = useContext(
    ApplicationContext
  );
  const { setUser } = userState;
  const { setError } = errorState;
  const { setLoading, loading } = loadingState;
  const { t } = useTranslation();
  return (
    <ViewWithModal>
      <Typography
        variant="h4"
        style={{
          margin: "0px 0px 20px",
        }}
      >
        {t("rg1")}
      </Typography>
      <Box display="flex" flexDirection="column" textAlign="center">
        <TextField
          variant="filled"
          placeholder={t("email")}
          style={{ margin: "10px" }}
          onChange={(e) => setEmail(e.target.value)}
          value={email}
          disabled={loading}
        />

        <TextField
          variant="filled"
          placeholder={t("password")}
          style={{ margin: "10px" }}
          onChange={(e) => setPassword(e.target.value)}
          value={password}
          disabled={loading}
          inputProps={{ type: "password" }}
        />

        <Button
          variant="contained"
          color="primary"
          style={{
            margin: "30px 0px 0px",
            height: "50px",
            background: "black",
            color: "white",
          }}
          onClick={() => {
            AuthService.register({
              email: email,
              password1: password,
              password2: password,
            })
              .then((value) => {
                setUser({
                  username: value.user.username,
                  email: value.user.email,
                });
                localStorage.user = JSON.stringify({
                  username: value.user.username,
                  email: value.user.email,
                });
                history.push(QUIZES);
              })
              .catch((value) => {
                setError(value);
              })
              .finally(() => setLoading(false));
            setLoading(true);
          }}
          disabled={loading || !password.length || !email.length}
        >
          {t("rg2")}
        </Button>
        <Link
          to={LOGIN}
          style={{
            margin: "20px 0px 30px",
          }}
          disabled={loading}
        >
          <Typography variant="body2">{t("rg3")}</Typography>
        </Link>
      </Box>
    </ViewWithModal>
  );
};

Register.propTypes = {
  name: PropTypes.string,
};

export default withRouter(Register);
