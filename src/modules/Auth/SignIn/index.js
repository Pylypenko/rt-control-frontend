import { Box, Button, TextField, Typography } from "@material-ui/core";
import PropTypes from "prop-types";
import React, { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { Link, useHistory } from "react-router-dom";
import { ApplicationContext } from "../../../App";
import ViewWithModal from "../../../common/ViewWithModal";
import { QUIZES, REGISTRATION } from "../../../routes";
import AuthService from "../../../services/Auth.service";

const SignIn = (props) => {
  const [email, setEmail] = useState("");
  const { userState, loadingState, errorState } = useContext(
    ApplicationContext
  );
  const { setUser } = userState;
  const [password, setPassword] = useState("");
  const { loading, setLoading } = loadingState;
  const { setError } = errorState;
  const { t } = useTranslation();
  const history = useHistory();
  return (
    <ViewWithModal>
      <Typography
        variant="h4"
        style={{
          margin: "0px 0px 20px",
        }}
      >
        {t("lgn1")}
      </Typography>
      <Box display="flex" flexDirection="column" textAlign="center">
        <TextField
          variant="filled"
          placeholder={t("email")}
          style={{ margin: "10px" }}
          onChange={(e) => setEmail(e.target.value)}
          value={email}
          disabled={loading}
        />

        <TextField
          variant="filled"
          placeholder={t("password")}
          style={{ margin: "10px" }}
          onChange={(e) => setPassword(e.target.value)}
          value={password}
          inputProps={{ type: "password" }}
          disabled={loading}
        />
        <Button
          variant="contained"
          color="primary"
          style={{
            margin: "30px 0px 30px",
            height: "50px",
            background: "black",
            color: "white",
          }}
          disabled={loading || !password.length || !email.length}
          onClick={() => {
            AuthService.login({ password, email })
              .then((value) => {
                setUser({
                  username: value.user.username,
                  email: value.user.email,
                });
                localStorage.user = JSON.stringify({
                  username: value.user.username,
                  email: value.user.email,
                });

                history.push(QUIZES);
              })
              .catch((value) => {
                setError(value);
              })
              .finally(() => setLoading(false));
            setLoading(true);
          }}
        >
          {t("lgn2")}
        </Button>
        <Link
          to={REGISTRATION}
          style={{
            margin: "0px 0px 30px",
          }}
          disabled={loading}
        >
          <Typography variant="body2">{t("lgn3")}</Typography>
        </Link>
      </Box>
    </ViewWithModal>
  );
};

SignIn.propTypes = {
  name: PropTypes.string,
};

export default SignIn;
