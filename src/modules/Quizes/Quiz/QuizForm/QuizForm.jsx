import { Box, Button, Dialog, Typography } from "@material-ui/core";
import React, { useContext, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory, withRouter } from "react-router-dom";
import { ApplicationContext } from "../../../../App";
import { QUIZES } from "../../../../routes";
import QuizService from "../../../../services/Quiz.service";
import Question from "../Question/Question";

const QuizForm = (props) => {
  const { currentQuizState, loadingState } = useContext(ApplicationContext);
  const currentQuiz = currentQuizState.currentQuiz || {};
  const { t } = useTranslation();
  const [showResultsModal, setShowResultsModal] = useState(false);
  const [currentQuestion, setCurrentQuestion] = useState(0);
  const { questions, title, quiz_token, id } = currentQuiz;

  const [questionsSnapshot, setQuestionsSnapshot] = useState(questions || []);

  const { setLoading } = loadingState;

  var a = (q) => {
    return q.answers.filter((a) => {
      return a.is_ticked;
    }).length;
  };
  const history = useHistory();
  if (!currentQuiz.questions) {
    history.push(QUIZES);
    return null;
  }
  return (
    <Box margin="50px">
      <Typography variant="h3">{title}</Typography>
      <Question
        onChange={(questionId, answerId, isTicked, answers_type) => {
          const changedQuestion = questionsSnapshot.find(
            (question) => question.id === questionId
          );
          changedQuestion.answers.forEach((answer) => {
            if (answers_type === "radio" || !answer.is_ticked) {
              answer.is_ticked = false;
            }

            if (answer.id === answerId) {
              answer.is_ticked = isTicked;
            }
          });
          const newSnapshot = questionsSnapshot.map((question) => {
            if (question.id === questionId) {
              return changedQuestion;
            }
            return question;
          });
          setQuestionsSnapshot(newSnapshot);
        }}
        {...questionsSnapshot[currentQuestion]}
      />

      <Typography variant="h5">{t("qf1")}</Typography>
      <Box display="flex">
        {questionsSnapshot.map((q, index) => {
          return (
            <Box
              width="50px"
              height="50px"
              bgcolor={a(q) ? "rgb(126, 255, 161)" : "white"}
              display="flex"
              alignItems="center"
              justifyContent="center"
              boxShadow="1px 5px 5px #888888"
              margin="10px"
              borderRadius="3px"
              style={{
                transition: "all 0.3s",
                filter:
                  index === currentQuestion
                    ? "brightness(0.9)"
                    : "brightness(1)",
              }}
              onClick={() => {
                setCurrentQuestion(index);
              }}
            >
              <Typography variant="body1">{index}</Typography>
            </Box>
          );
        })}
      </Box>
      <Button
        disabled={currentQuestion === 0}
        onClick={() => {
          setCurrentQuestion(currentQuestion - 1);
        }}
      >
        {t("qf2")}
      </Button>
      <Button
        disabled={currentQuestion === questionsSnapshot.length - 1}
        onClick={() => setCurrentQuestion(currentQuestion + 1)}
      >
        {t("qf3")}
      </Button>

      <Button
        onClick={() => {
          setLoading(true);
          QuizService.submitQuiz({
            id,
            data: { quiz_token, questions_snapshot: questionsSnapshot },
          })
            .then((response) => {
              setShowResultsModal(response.body);
              // end_time: "2020-05-31T08:00:00Z";
              // id: "4";
              // start_time: "2020-05-06T00:00:00Z";
              // title: "PZPI-17-3 TEST";
              // __proto__: Object;
              // total_result: 40;
            })
            .catch((error) => {
              debugger;
            })
            .finally(() => {
              setLoading(false);
            });
        }}
        style={{ background: "rgb(126, 255, 161)" }}
      >
        {t("qf4")}
      </Button>
      {showResultsModal && (
        <Dialog
          open={true}
          onClose={() => {
            localStorage.removeItem("currentQuiz");
            setShowResultsModal(false);
            history.push(QUIZES);
          }}
        >
          <Box style={{ padding: "50px", background: "rgb(126, 255, 161)" }}>
            <Typography variant="h4"> {t("qf8")}</Typography>
            <Typography variant="h5" style={{ marginTop: "30px" }}>
              {t("qf5")}
            </Typography>

            <Typography variant="body2" style={{ marginTop: "50px" }}>
              {t("qf6")} {showResultsModal.quiz.title}.
            </Typography>

            <Typography variant="h5" style={{ marginTop: "20px" }}>
              {t("qf7")} {showResultsModal.total_result}.
            </Typography>
          </Box>
        </Dialog>
      )}
    </Box>
  );
};

export default withRouter(QuizForm);
