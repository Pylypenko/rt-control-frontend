import { Box, Button, Paper, Typography } from "@material-ui/core";
import moment from "moment";
import React, { useContext } from "react";
import { useTranslation } from "react-i18next";
import { useHistory, withRouter } from "react-router-dom";
import { ApplicationContext } from "../../../App";
import { QUIZES } from "../../../routes";
import QuizService from "../../../services/Quiz.service";

const QuizItem = (props) => {
  const { end_time, start_time, title, id } = props;
  const { t } = useTranslation();

  const { loadingState, errorState, currentQuizState } = useContext(
    ApplicationContext
  );

  const { setLoading } = loadingState;
  const { setError } = errorState;
  const { setCurrentQuiz } = currentQuizState;
  const history = useHistory();
  return (
    <>
      <Paper
        elevation={4}
        style={{ minWidth: "300px", minHeight: "400px", padding: "50px" }}
      >
        <Typography variant="h3">{title}</Typography>

        <Typography variant="body2" style={{ marginTop: "30px" }}>
          {t("qi1")} {moment(start_time).format("DD MMM YYYY HH:mm")}
        </Typography>

        <Typography variant="body2" style={{ marginTop: "30px" }}>
          {t("qi2")} {moment(end_time).format("DD MMM YYYY HH:mm")}
        </Typography>

        <Box
          display="flex"
          width="100%"
          justifyContent="space-between"
          marginTop="20px"
        >
          <Button
            onClick={() => {
              if (!localStorage.currentQuiz) {
                setLoading(true);
                QuizService.startQuiz(id)
                  .then((response) => {
                    setCurrentQuiz(response.body);
                    localStorage.currentQuiz = JSON.stringify(response.body);
                    history.push(`${QUIZES}/${id}`);
                  })
                  .catch((error) => setError(error))
                  .finally(() => setLoading(false));
                return;
              }
              history.push(`${QUIZES}/${id}`);
            }}
          >
            {t("qi3")}
          </Button>
        </Box>
      </Paper>
    </>
  );
};

export default withRouter(QuizItem);
