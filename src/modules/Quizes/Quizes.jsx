import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  FormControlLabel,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Typography,
} from "@material-ui/core";
import Alert from "@material-ui/lab/Alert";
import Skeleton from "@material-ui/lab/Skeleton";
import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { useHistory, withRouter } from "react-router-dom";
import { ApplicationContext } from "../../App";
import QuizService from "../../services/Quiz.service";
import QuizItem from "./Quiz/QuizItem";

const Quizes = (props) => {
  const [quizes, setQuizes] = useState([
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
    {},
  ]);
  const [takenQuizes, setTakenQuizes] = useState([]);
  const { userState, loadingState, errorState } = useContext(
    ApplicationContext
  );
  const { setLoading } = loadingState;
  const { setError } = errorState;

  const { user } = userState;

  const [itemChanged, setItemChanged] = useState(false);
  const [openReport, setOpenReport] = useState(false);
  const [reason, setReason] = useState("");
  const [reportedQuiz, setReportedQuiz] = useState(false);
  const [reportSuccess, setReportSuccess] = useState(false);
  const { t } = useTranslation();

  const renderItems = (items) => {
    if (!items.length) {
      return t("qzs1");
    }
    return items.map((item) => {
      return (
        <Grid
          item
          xs={12}
          sm={12}
          md={6}
          lg={4}
          xl={3}
          key={Math.random() * 100000}
        >
          {!item.id ? (
            <Skeleton variant="rect" width={"100%"} height={400} />
          ) : (
            <QuizItem
              onChange={() => {
                setItemChanged((prevstate) => !prevstate);
              }}
              key={item.id}
              {...item}
            />
          )}
        </Grid>
      );
    });
  };
  useEffect(() => {
    var a = async () => {
      try {
        setLoading(true);
        const response = await QuizService.getQuizes();
        const takenQuizes = await QuizService.getTakenQuizes();
        if (!response.body.items.length && localStorage.currentQuiz) {
          response.body.items.push(JSON.parse(localStorage.currentQuiz));
        }
        setQuizes(response.body.items || []);
        let tq = takenQuizes.body.items;
        for (let i = 0; i < tq.length; i++) {
          if (tq.map((q) => q.quiz.id).lastIndexOf(tq[i].quiz.id) !== i) {
            tq.splice(i, 1);
            i--;
          }
        }
        setTakenQuizes(tq);
      } catch (e) {
        setError(e);
      } finally {
        setLoading(false);
      }
    };
    a();
  }, [itemChanged, setError, setLoading]);

  const history = useHistory();

  if (!user) {
    history.push("/registration");
    return null;
  }
  return (
    <>
      <Grid container>
        <Box display="flex" style={{ margin: "60px" }} width="100%">
          <Box
            display="flex"
            justifyContent="space-between"
            width="90%"
            margin="auto"
          >
            <Box
              display="flex"
              flexDirection="column"
              justifyContent="space-between"
            >
              <Typography variant="h3">{t("qzs2")}</Typography>
              {takenQuizes.length > 0 && (
                <Button
                  onClick={() => setOpenReport(true)}
                  style={{
                    marginTop: "30px",
                    background: "black",
                    color: "rgb(126, 255, 161)",
                  }}
                >
                  {t("qzs3")}
                </Button>
              )}
            </Box>
          </Box>
        </Box>
        <Grid
          style={{ width: "calc(100vw - 100px)", margin: "auto" }}
          container
          spacing={10}
        >
          {renderItems(quizes)}
        </Grid>
      </Grid>
      {openReport && (
        <Dialog open={openReport} onClose={() => setOpenReport(false)}>
          <DialogContent>
            <Box style={{ padding: "50px" }}>
              <Typography variant="h5">{t("qzs4")}</Typography>
              <Box style={{ margin: "50px 0px" }}>
                <RadioGroup value={reportedQuiz}>
                  {takenQuizes.map((tq) => (
                    <FormControlLabel
                      label={tq.quiz.title}
                      onChange={(e) => setReportedQuiz(e.target.value)}
                      control={<Radio value={tq.quiz.id} />}
                    />
                  ))}
                </RadioGroup>
              </Box>
              <Typography variant="body1" style={{ marginBottom: "30px" }}>
                {t("qzs5")}
              </Typography>
              <TextField
                onChange={(e) => setReason(e.target.value)}
                placeholder="Reason"
                multiline
              />
            </Box>
          </DialogContent>
          <DialogActions>
            <Button
              style={{ color: "white", background: "black" }}
              onClick={() => setOpenReport(false)}
            >
              {t("qzs6")}
            </Button>
            <Button
              style={{ color: "black", background: "rgb(126, 255, 161)" }}
              onClick={() => {
                setLoading(true);
                const reportData = { id: reportedQuiz, data: { reason } };

                QuizService.submitReport(reportData)
                  .then(() => {
                    setReportSuccess(true);
                    setOpenReport(false);
                  })
                  .catch((er) => setError(er))
                  .finally(() => setLoading(false));
              }}
            >
              {t("qzs7")}
            </Button>
          </DialogActions>
        </Dialog>
      )}
      {reportSuccess && (
        <Alert
          onClose={() => {
            setReportSuccess(false);
          }}
          severity="success"
          variant="filled"
          style={{
            position: "fixed",
            bottom: "10px",
            width: "50%",
            left: "50%",
            transform: "translateX(-50%)",
            background: "rgb(126,255,161)",
            color: "black",
          }}
        >
          {t("qzs8")}
        </Alert>
      )}
    </>
  );
};
export default withRouter(Quizes);
