import {
  Box,
  Button,
  createMuiTheme,
  LinearProgress,
  Paper,
  Popover,
  Typography,
} from "@material-ui/core";
import { grey } from "@material-ui/core/colors";
import { ThemeProvider } from "@material-ui/core/styles";
import Alert from "@material-ui/lab/Alert";
import AlertTitle from "@material-ui/lab/AlertTitle";
import * as Sentry from "@sentry/browser";
import i18n from "i18next";
import React, { useEffect, useState } from "react";
import { initReactI18next } from "react-i18next";
import { BrowserRouter as Router, Link, Route, Switch } from "react-router-dom";
import "./App.css";
import english from "./assets/locales/english.json";
import ukr from "./assets/locales/ukrainian.json";
import "./assets/styles/overrides.scss";
import { CLIENT_ROUTES } from "./routes";
import ApiService from "./services/Api.service";
import AuthService from "./services/Auth.service";
if (process.env.REACT_APP_SENTRY_URL) {
  Sentry.init({ dsn: process.env.REACT_APP_SENTRY_URL });
}
const theme = createMuiTheme({
  palette: {
    primary: grey,
    secondary: {
      main: "rgb(126, 255, 161)",
    },
  },
});
export const ApplicationContext = React.createContext({});

const App = () => {
  const [locale, setLocale] = useState("en");
  i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
      resources: {
        en: {
          translation: english,
        },
        ukr: {
          translation: ukr,
        },
      },
      lng: locale,

      keySeparator: false, // we do not use keys in form messages.welcome

      interpolation: {
        escapeValue: false, // react already safes from xss
      },
    });
  const [user, setUser] = useState("");
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(false);
  const [currentQuiz, setCurrentQuiz] = useState(false);
  const [loadingClient, setLoadingClient] = useState(true);
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    if (loading) {
      return;
    }
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "simple-popover" : undefined;
  const usr = user || (localStorage.user && JSON.parse(localStorage.user));
  const curQuiz =
    currentQuiz ||
    (localStorage.currentQuiz && JSON.parse(localStorage.currentQuiz));

  const getErrorAlertValues = () => {
    return !error.response.body.detail ? (
      Object.values(error.response.body).map((errorsArr) =>
        errorsArr.map((error) => (
          <Typography variant="body1" key={Math.random() * 100000}>
            {error}
          </Typography>
        ))
      )
    ) : (
      <Typography variant="body1">{error.response.body.detail}</Typography>
    );
  };
  const er = error && getErrorAlertValues();

  useEffect(() => {
    new ApiService().init().then(() => {
      if (window.client) {
        setLoadingClient(false);
      }
    });
  }, []);

  return (
    <ApplicationContext.Provider
      value={{
        userState: {
          user: usr,
          setUser,
        },
        loadingState: {
          loading,
          setLoading,
        },
        errorState: {
          error,
          setError,
        },
        currentQuizState: {
          currentQuiz: curQuiz,
          setCurrentQuiz,
        },
      }}
    >
      <ThemeProvider theme={theme}>
        {loadingClient ? (
          <>
            <LinearProgress
              variant="query"
              style={{ height: "7px", position: "fixed", width: "100%" }}
            />
            <Typography
              variant="h2"
              style={{
                position: "absolute",
                top: "50%",
                left: "50%",
                transform: "translate(-50%,-50%)",
                color: "#d0d0d0",
              }}
            >
              Please wait, API is loading...
            </Typography>
          </>
        ) : (
          <Router>
            {loading && (
              <LinearProgress
                variant="query"
                style={{ height: "7px", position: "fixed", width: "100%" }}
              />
            )}

            <Box
              display="flex"
              boxShadow="0 5px 10px #f1f1f1"
              style={{
                padding: "20px 50px",
              }}
              justifyContent="space-between"
            >
              <Typography variant="h5">
                {process.env.REACT_APP_PROJECT_NAME || "Project Name"}
              </Typography>

              {usr && (
                <>
                  <Box display="flex">
                    <Typography variant="h5" onClick={handleClick}>
                      {usr.username}
                    </Typography>
                  </Box>
                  <Popover
                    id={id}
                    open={open}
                    anchorEl={anchorEl}
                    onClose={handleClose}
                    anchorOrigin={{
                      vertical: "bottom",
                      horizontal: "center",
                    }}
                    transformOrigin={{
                      vertical: "top",
                      horizontal: "center",
                    }}
                  >
                    <Paper
                      style={{
                        width: "100px",
                        padding: "10px",
                      }}
                    >
                      <Link
                        to="/"
                        onClick={() => {
                          AuthService.logout()
                            .then(() => {
                              handleClose();

                              setUser(false);
                            })
                            .catch((value) => {
                              setError(value);
                            })
                            .finally(() => setLoading(false));
                          setLoading(true);
                        }}
                      >
                        <Typography variant="body1">Log out</Typography>
                      </Link>
                    </Paper>
                  </Popover>
                </>
              )}
              <Box>
                <Button onClick={() => setLocale("en")}>EN</Button>
                <Button onClick={() => setLocale("ukr")}>UKR</Button>
              </Box>
            </Box>
            <Switch>
              {CLIENT_ROUTES.map(({ route, component, exact }) => (
                <Route
                  exact={exact}
                  path={route}
                  component={component}
                  key={route}
                />
              ))}
            </Switch>
            {error && (
              <Alert
                variant="filled"
                severity="error"
                onClose={() => {
                  setError(false);
                }}
                style={{
                  position: "fixed",
                  bottom: "10px",
                  width: "50%",
                  left: "50%",
                  transform: "translateX(-50%)",
                }}
              >
                <AlertTitle>{error.message}</AlertTitle>
                {er}
              </Alert>
            )}
          </Router>
        )}
      </ThemeProvider>
    </ApplicationContext.Provider>
  );
};

export default App;
