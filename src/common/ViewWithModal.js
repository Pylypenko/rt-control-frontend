import React from "react";
import PropTypes from "prop-types";

import { Grid, Paper, Typography } from "@material-ui/core";
import { withRouter } from "react-router-dom";
import { useTranslation } from "react-i18next";
const ViewWithModal = (props) => {
  const { t } = useTranslation();
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        minHeight: "calc(100vh - 72px)",
        maxWidth: "calc(100vw - 72px)",
      }}
    >
      <Grid container justify="center" alignItems="center">
        <Grid item xs={4}>
          <Paper
            elevation={0}
            style={{
              minHeight: "500px",
              textAlign: "left",
              padding: "50px",
            }}
          >
            <Typography variant="h4">{t("vwm1")}</Typography>
            <Typography variant="body1" style={{ marginTop: "30px" }}>
              {t("vwm2")}
            </Typography>
            <Typography variant="body1" style={{ marginTop: "30px" }}>
              {t("vwm3")}
            </Typography>
            <Typography variant="body1" style={{ marginTop: "30px" }}>
              {t("vwm4")}
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={4}>
          <Paper
            elevation={8}
            style={{
              // minHeight: "500px",
              minWidth: "400px",
              textAlign: "left",
              padding: "50px",
              background: "rgb(126, 255, 161)",
            }}
          >
            {props.children}
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

ViewWithModal.propTypes = {
  name: PropTypes.string,
};

export default withRouter(ViewWithModal);
